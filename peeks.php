<?php
session_start();
include "rest.php";

?>
<!DOCTYPE html>
<html lang="en" class="app">
  <head>  
    <meta charset="utf-8" />
    <title>Chattr | Admin Panel</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />  
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
    <link rel="stylesheet" href="js/jvectormap/jquery-jvectormap-1.2.2.css" type="text/css" />
    <script src="js/sorttable.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

  </head>
  <body>

    <section class="vbox">
      <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
        <div class="navbar-header aside-md dk">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
          <i class="fa fa-bars"></i>
          </a>
          <a href="index.php" class="navbar-brand">
          <img src="images/logo.png" class="m-r-sm" alt="scale">
          <span class="hidden-nav-xs">Chattr</span>
          </a>
          <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
          <i class="fa fa-cog"></i>
          </a>
          </div>
     
          <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
                </span>
                <input type="text" class="form-control input-sm no-border" placeholder="Search">            
              </div>
            </div>
          </form>
           <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="thumb-sm avatar pull-left">
                  <img src="images/a0.png" alt="...">
                  </span>
                  <?php
echo $_SESSION['name']; ?>
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInRight">            
                  <li>
                    <a href="signin.php" >Logout</a>
                  </li>
                </ul>
              </li>
              </ul>      
            </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">          
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <div class="clearfix wrapper dk nav-user hidden-xs">
                  <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="thumb avatar pull-left m-r">                        
                        <img src="images/a0.png" class="dker" alt="...">
                        <i class="on md b-black"></i>
                      </span>
                      <span class="hidden-nav-xs clear">
                        <span class="block m-t-xs">
                          <strong class="font-bold text-lt">
                          <?php
echo $_SESSION['name'];
?>
                          </strong> 
                          <b class="caret"></b>
                        </span>  
                      </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">                      
                       <li>
                        <a href="signin.php">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>                

                <!-- nav -->                 
                <nav class="nav-primary hidden-xs">
                  <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
                  <ul class="nav nav-main" data-ride="collapse">
                    <li>
                      <a href="index.html" class="auto">
                        <i class="i i-statistics icon">
                        </i>
                        <span class="font-bold">Overview</span>
                      </a>
                    </li>         
                        <li >
                          <a href="users.php" class="auto">                               
                            <i class="i i-user3"></i>
                            <span>Users</span>
                          </a>
                        </li>
                        <li >
                          <a href="posts.php" class="auto">                                                        
                            <i class="i i-chat3"></i>
                            <span>Chattrs</span>
                          </a>
                        </li>
                        <li class="active">
                          <a href="peeks.php" class="auto">                            
                            <i class="fa fa-star-o"></i>
                            <span>Peeks</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
            <footer class="footer hidden-xs no-padder text-center-nav-xs">
              <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                <i class="i i-logout"></i>
              </a>
              <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                <i class="i i-circleleft text"></i>
                <i class="i i-circleright text-active"></i>
              </a>
            </footer>
          </section>
        </aside>
        <!-- /.aside -->
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
            <section class="vbox">
            <section class="scrollable wrapper">
              <div class="panel b-a">
                <div class="panel-heading b-b">Peeks of the day               
                  </div>
                        <div class="text-right text-left-xs">
                      <form role="form" method = 'POST' action = 'rest.php' >
                    <input type="text" id="address" style="width: 500px;"></input>
           <input type="hidden" id="city2" name="city2" />
            <input type="hidden" id="cityLat" name="cityLat" />
               <input type="hidden" id="cityLng" name="cityLng"/>
             <input type='submit' class='btn btn-success' value='Add' name='addlocation' /></input>
        </form>          
                      </div>
                <div class="panel-body">
                  <div class="h3 m-b font-thin">Hot Locations</div>
                  <div class="row">
                    <?php
$totalposts = getNumbers(POST_API_URL);
$url = "http://45.55.229.81/chattr/api/v1/locations";
$cht = curl_init($url);
curl_setopt($cht, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($cht);
$loc = json_decode($response);
curl_close($cht);

foreach($loc as $item)
  {
  $name = $item->{'name'};
  $lat = $item->{'lat'};
  $lng = $item->{'lng'};
        $purl = "http://45.55.229.81/chattr/api/v1/posts/all";
        $client = curl_init($purl);
        curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($client);
        $result = json_decode($response);
        curl_close($client);
        $uurl = "http://45.55.229.81/chattr/api/v1/users";
        $client = curl_init($uurl);
        curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($client);
        $users = json_decode($response);
        curl_close($client);
        $i = 0;
        $y = 0;
        foreach($result as $plocation)
          {
          $plat = $plocation->{'lat'};
          $plng = $plocation->{'lng'};
              $distance = getDistance($lat, $lng, $plat, $plng);
              if ($distance < 4)
                {
                $i = $i + 1;
                }
                else
                {
                $y = $y + 1;
                }
              }

            $x = 0;
            $z = 0;
            foreach($users as $ulocation)
              {
              $ulat = $ulocation->{'lat'};
              $ulng = $ulocation->{'lng'};
              $distance = getDistance($lat, $lng, $ulat, $ulng);
                  if ($distance < 4)
                    {
                    $x = $x + 1;
                    }
                    else
                    {
                    $z = $z + 1;
                    }
                  }

                echo '<section class="panel clearfix bg-default dk">';
                echo '<div class="panel-body">';
                echo "<div class='col-md-2'>";
                echo '<h4>' . $name . '</h4><br/>';
                echo '</div>';
                echo "<div class='col-md-2'>";
                echo '<h4>' . $i . ' <small><i class="i i-chat3"></i></small></h4>';
                echo "</div>";
                $ratio1 = ceil(($i / $totalposts) * 100);
                echo "<div class='col-md-2'>";
                echo '<h4>' . $ratio1 . '% <small>of all chattrs</small></h4>';
                echo "</div>";
                echo "<div class='col-md-2'>";
                echo '<h4>' . $x . ' <small><i class="i i-users2"></i></small></h4>';
                echo "</div>";
                echo "<div class='col-md-2'>";
                echo "<form method='POST' action='rest.php?id=" . $item->{'id'} . "' >";
                  echo "<button name = 'deletelocation' type='submit' class='btn btn-default'><i class='fa fa-trash-o'></i></button>";
                  echo "</form>";
                  echo "</div>";
                  echo " <div class='clear'></div>";
                  echo "</div>";
                  echo "</section>";
                  }

?>                   
              </section>
            </section>
            </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
 
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>  
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="js/charts/flot/jquery.flot.min.js"></script>
  <script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="js/charts/flot/jquery.flot.spline.js"></script>
  <script src="js/charts/flot/jquery.flot.pie.min.js"></script>
  <script src="js/charts/flot/jquery.flot.resize.js"></script>
  <script src="js/charts/flot/jquery.flot.grow.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-us-aea-en.js"></script> 
  <script src="js/calendar/bootstrap_calendar.js"></script>
  <script src="js/sortable/jquery.sortable.js"></script>
  <script src="js/app.plugin.js"></script>
   <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script>
      <script type="text/javascript">
        function initialize() {
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('city2').value = place.name;
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
    </script>
      
  </body>
</html>