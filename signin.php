
<?php
session_start();
$con = mysqli_connect('localhost', 'root', '', 'lara') or die("Failed to connect to MySQL dbhost: " . mysql_error());

if (isset($_POST['submit'], $_POST['email'], $_POST['password']))
  {
  if (!empty($_POST['email'] && $_POST['password']))
    {
    $email = $_POST['email'];
    $pass = $_POST['password'];
    $email = stripslashes($email);
    $pass = stripslashes($pass);
    $email = mysqli_real_escape_string($email);
    $pass = mysqli_real_escape_string($pass);
    $sql = "SELECT *  FROM users where email = '$_POST[email]' AND password = '$_POST[password]'";
    $query = mysqli_query($con, $sql) or die(mysqli_error($con));
    if (mysqli_num_rows($query) > 0)
      {
      session_start();
      while ($row = mysqli_fetch_assoc($query))
        {
        $_SESSION['name'] = $row["firstname"] . " " . $row["lastname"];
        header('Location: index.php');
        exit;
        }

      $_SESSION['error'] = "";
      }
      else
      {
      $_SESSION['error'] = "Username or Password Incorrect";
      header("Location:signin.php");
      exit;
      }
    }
    else
    {
    $_SESSION['error'] = "Username and/or Password missing";
    header("Location:signin.php");
    exit;
    }
  }

mysqli_close($con);
?>

<html lang="en" class="app">
  <head>
    <meta charset="utf-8" />
    <title>Scale | Web Application</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
  </head>
  <body class="">
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
      <div class="container aside-xl">
      <a class="navbar-brand block" href="index.html">Chattr</a>
      <section class="m-b-lg">
        <header class="wrapper text-center">
          <strong>Sign in to get access</strong>
        </header>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
          <div class="list-group">
            <div class="list-group-item">
              <input type="email" placeholder="Email" class="form-control no-border" name='email'>
            </div>
            <div class="list-group-item">
               <input type="password" placeholder="Password" class="form-control no-border" name = "password">
            </div>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block" name="submit">Sign in</button>
          <div class="text-center m-t m-b"><a href="#"><small>Forgot password?</small></a></div>
          <div class="text-center"> <?php

if (isset($_SESSION['error']))
  {
  echo $_SESSION['error'];
  } ?>

         </div>
          <div class="line line-dashed"></div>
        </form>
      </section>
      </div>
    </section>
    <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <p>
        <small>Admin panel for chattr app <br />&copy; 2015</small>
      </p>
    </div>
  </footer>
  <!-- / footer -->
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/app.plugin.js"></script>
</body>
</html>