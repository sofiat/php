<?php
define("USERS_API_URL", "http://45.55.229.81/chattr/api/v1/users");
define("POST_API_URL", "http://45.55.229.81/chattr/api/v1/posts/all");
define("POSTS_API_URL", "http://45.55.229.81/chattr/api/v1/posts");
define("REPLIES_API_URL", "http://45.55.229.81/chattr/api/v1/replies");
define("LOCATIONS_API_URL", "http://45.55.229.81/chattr/api/v1/locations");
define("hoursAgo", "http://45.55.229.81/chattr/api/v1/users?time_ago=4");
define("dayAgo", "http://45.55.229.81/chattr/api/v1/users?time_ago=24");
define("weekAgo", "http://45.55.229.81/chattr/api/v1/users?time_ago=168");
define("monthAgo", "http://45.55.229.81/chattr/api/v1/users?time_ago=720");
define("DB_HOST", "localhost");

if (isset($_POST['unbanpost']))
  {
  unban(POSTS_API_URL);
  header('Location: posts.php');
  }

if (isset($_POST['unbanuser']))
  {
  unban(USERS_API_URL);
  header('Location: users.php');
  }

if (isset($_POST['unbanreply']))
  {
  unban(REPLIES_API_URL);
  header('Location: reply.php');
  }

if (isset($_POST['banpost']))
  {
  ban(POSTS_API_URL);
  header('Location: posts.php');
  }

if (isset($_POST['banuser']))
  {
  ban(USERS_API_URL);
  header('Location: users.php');
  }

if (isset($_POST['banreply']))
  {
  ban(REPLIES_API_URL);
  header('Location: reply.php');
  }

if (isset($_POST['deleteuser']))
  {
  delete(USERS_API_URL);
  header('Location: users.php');
  }

if (isset($_POST['deletereply']))
  {
  delete(REPLIES_API_URL);
  header('Location: reply.php');
  }

if (isset($_POST['deletepost']))
  {
  deletePost(USERS_API_URL);
  header('Location: posts.php');
  }

if (isset($_POST['addlocation']))
  {
  addLocation(LOCATIONS_API_URL);
  header('Location: peeks.php');
  }

if (isset($_POST['deletelocation']))
  {
  deleteLocation(LOCATIONS_API_URL);
  header('Location: peeks.php');
  }

function getNumbers($url)
  {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($ch);
  $answer = json_decode($response);
  $number = sizeof($answer, 1);
  return $number;
  curl_close($ch);
  }

function ban($url)
  {
  global $id;
  $id = $_GET['id'];
  $client = curl_init($url . "/" . $id . "/ban");
  curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($client);
  $info = json_decode($response);
  curl_close($client);
  }

function unban($url)
  {
  global $id;
  $id = $_GET['id'];
  $cht = curl_init($url . "/" . $id . "/unban");
  curl_setopt($cht, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($cht);
  $info = json_decode($response);
  curl_close($cht);
  }

function delete($url)
  {
  global $id;
  $id = $_GET['id'];
  $curl = curl_init($url . "/" . $id);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json'
  ));

  // Make the REST call, returning the result

  $response = curl_exec($curl);
  if (!$response)
    {
    die("Connection Failure.n");
    }
  }

function deletePost($url)
  {
  global $id;
  global $uid;
  $id = $_GET['id'];
  $uid = $_GET['uid'];
  $curl = curl_init($url . "/" . $uid . "/posts/" . $id);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json'
  ));

  // Make the REST call, returning the result

  $response = curl_exec($curl);
  if (!$response)
    {
    die("Connection Failure.n");
    }
  }

function addLocation($url)
  {
  $name = $_POST['city2'];
  $lat = $_POST['cityLat'];
  $lng = $_POST['cityLng'];
  $data = array(
    "name" => "$name",
    "lat" => "$lat",
    "lng" => "$lng"
  );
  $data_string = json_encode($data);
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string)
  ));
  $result = curl_exec($ch);
  }

function deleteLocation($url)
  {
  global $id;
  $id = $_GET['id'];
  $curl = curl_init($url . "/" . $id);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json'
  ));

  // Make the REST call, returning the result

  $response = curl_exec($curl);
  if (!$response)
    {
    die("Connection Failure.n");
    }
  }

function getDistance($latitude1, $longitude1, $latitude2, $longitude2)
  {
  $earth_radius = 6371;
  $dLat = deg2rad($latitude2 - $latitude1);
  $dLon = deg2rad($longitude2 - $longitude1);
  $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
  $c = 2 * asin(sqrt($a));
  $d = $earth_radius * $c;
  return $d;
  }

?>