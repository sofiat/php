<?php
session_start();
include "rest.php";

?>

<!DOCTYPE html>
<html lang="en" class="app">
  <head>  
    <meta charset="utf-8" />
    <title>Chattr | Admin Panel</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />  
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
    <script src="js/sorttable.js"></script>
    <script src="js/moment.js"></script>
    <script>
    $(document).ready(function(){ 
    $('.location').each(function(){
    var time = $(this).html();
    var cal = moment(time, 'YYYY-MM-DD hh:mm:ss').fromNow();
    $(this).html(cal);
    });
   });
   </script>
 
    </head>
  <body class="">
    <section class="vbox">
     <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
        <div class="navbar-header aside-md dk">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"><i class="fa fa-bars"></i></a>
          <a href="index.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm" alt="scale">
          <span class="hidden-nav-xs">Chattr</span>
          </a>
          <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"><i class="fa fa-cog"></i></a>
        </div>
     
      <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
            </span>
            <input type="text" class="form-control input-sm no-border" placeholder="Search">            
          </div>
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-left">
              <img src="images/a0.png" alt="...">
            </span>
            <?php
echo $_SESSION['name']; ?> <b class="caret"></b>
          </a>
        <ul class="dropdown-menu animated fadeInRight">               
          <li>
            <a href="signin.php" >Logout</a>
          </li>
          </ul>
        </li>
      </ul>      
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">          
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <div class="clearfix wrapper dk nav-user hidden-xs">
                  <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-nav-xs clear">
                        <span class="block m-t-xs">
                          <strong class="font-bold text-lt"><?php
echo $_SESSION['name']; ?></strong> 
                          <b class="caret"></b>
                        </span>         
                      </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">                      
                      <li>
                        <a href="signin.php">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>                
                <!-- nav -->                 
                <nav class="nav-primary hidden-xs">
                  <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
                    <ul class="nav nav-main" data-ride="collapse">
                      <li >
                        <a href="index.php" class="auto">
                        <i class="i i-statistics icon"></i>
                        <span class="font-bold">Overview</span>
                        </a>
                      </li>
                      <li>
                        <a href="users.php" class="auto"><i class="fa fa-users"></i><span>Users</span></a>
                      </li>
                      <li class='active'>
                        <a href="posts.php" class="auto"><i class="i i-chat3"></i><span>Chattrs</span></a>
                      </li>
                      <li >
                        <a href="peeks.php" class="auto"><i class="fa fa-star-o"></i><span>Peeks</span></a>
                      </li> 
                      </ul>
                    </li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
            <footer class="footer hidden-xs no-padder text-center-nav-xs">
              <a href="signin.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                <i class="i i-logout"></i>
              </a>
              <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                <i class="i i-circleleft text"></i>
                <i class="i i-circleright text-active"></i>
              </a>
            </footer>
          </section>
        </aside>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Chattrs</h3>
                    </div>
                    <div class="col-sm-6 text-right text-left-xs m-t-md">                    
                      <a href="#nav, #sidebar" class="btn btn-icon b-2x btn-info btn-rounded" data-toggle="class:nav-xs, show"><i class="fa fa-bars"></i></a>
                    </div>
                    
                  </section>
                  </div>           
                  <div class="row">
                    <div class="col-md-12">
                    <section class="panel panel-default">
                      <header class="panel-heading">
                      User Posts
                      </header>

                      <?php

if (isset($_GET['id']))
  {
  $_SESSION['id'] = $_GET['id'];
  }

$url = "http://45.55.229.81/chattr/api/v1/posts/" . $_SESSION['id'] . "/replies";
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response);
curl_close($client);
echo '<div class="table-responsive">';
echo '<table class="table table-striped m-b-none sortable">';
echo '<thead>
                       <tr>
                          <th width="15%">Date</th>
                          <th width="40%">Reply</th> 
                          <th width="15%">Username</th>
                          <th width="10%">Status</th>
                          <th width="10%">Ban/Unban</th>
                          <th width="10%">Delete</th>
                                               
                        </tr>
                        </thead>
                        <tbody>';

foreach($result as $item)
  {
  $name = $item->{'user'}->{'name'};
      $time = $item->{'timePosted'};
        $pid = $item->{'postId'};
          echo "<tr><td width='15%' class='location'>" . date('Y-m-d h:i:sa', strtotime($item->{'timePosted'})) . "</td>";
            echo "<td>" . $item->{'body'} . "</td>";
              echo "<td>" . $item->{'user'}->{'name'} . "</td>";
                  echo "<form method='POST' action='rest.php?id=" . $item->{'postId'} . "' id='form2'>";
                    if ($item->{'banned'} == 1)
                        {
                        echo "<td>banned</td>";
                        echo "<td><button type='submit' name='unbanreply' class='btn btn-default' id='form2'><i class= 'fa fa-unlock'></button></td>";
                        }
                        else
                        {
                        echo "<td> active</td>";
                        echo "<td><button type='submit' name='banreply' class='btn btn-default' id='form2'><i class= 'fa fa-unlock-alt'></button></td>";
                        }

                      echo "<td><button type='submit' name='deletereply' class='btn btn-default' id='form2'><i class= 'fa fa-trash-o'></td></tr>";
                      echo "</form>";
                      }

                    echo "</table>";
?>
                      </section>
                    </div>
                  </div>  
              </section>
            </section>
            <!-- side content -->
            <!-- / side content -->
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>  
 
  <script src="js/extra.js"></script>  
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="js/charts/flot/jquery.flot.min.js"></script>
  <script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="js/charts/flot/jquery.flot.spline.js"></script>
  <script src="js/charts/flot/jquery.flot.pie.min.js"></script>
  <script src="js/charts/flot/jquery.flot.resize.js"></script>
  <script src="js/charts/flot/jquery.flot.grow.js"></script>
  <script src="js/charts/flot/demo.js"></script>
  <script src="js/calendar/bootstrap_calendar.js"></script>
  <script src="js/calendar/demo.js"></script>

  <script src="js/sortable/jquery.sortable.js"></script>
  <script src="js/app.plugin.js"></script>
 
  </body>
</html>