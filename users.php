<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en" class="app">
  <head>
    <meta charset="utf-8" />
    <title>Chattr | Admin Panel</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
    <script src="js/sorttable.js"></script>
  </head>
  <body>
    <section class="vbox">
      <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
          <div class="navbar-header aside-md dk">
            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
            <i class="fa fa-bars"></i>
            </a>
            <a href="index.php" class="navbar-brand">
            <img src="images/logo.png" class="m-r-sm" alt="scale">
            <span class="hidden-nav-xs">Chattr</span>
            </a>
            <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
           <i class="fa fa-cog"></i>
           </a>
          </div>
          <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
            <div class="form-group">
            <div class="input-group">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
            </span>
            <input type="text" class="form-control input-sm no-border" placeholder="Search">
            </div>
          </div>
          </form>
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-left">
              <img src="images/a0.png" alt="...">
              </span>
            <?php
echo $_SESSION['name']; ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu animated fadeInRight">
            <li>
              <a href="signin.html" >Logout</a>
            </li>
          </ul>
        </li>
      </ul>
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <div class="clearfix wrapper dk nav-user hidden-xs">
                  <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="thumb avatar pull-left m-r">
                        <img src="images/a0.png" class="dker" alt="...">
                        <i class="on md b-black"></i>
                      </span>
                      <span class="hidden-nav-xs clear">
                        <span class="block m-t-xs">
                          <strong class="font-bold text-lt"><?php
echo $_SESSION['name']; ?></strong>
                          <b class="caret"></b>
                        </span>
                      </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                      <li>
                        <a href="signin.html">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
                  <ul class="nav nav-main" data-ride="collapse">
                    <li >
                      <a href="index.php" class="auto">
                        <i class="i i-statistics icon">
                        </i>
                        <span class="font-bold">Overview</span>
                      </a>
                    </li>
                        <li class="active" >
                          <a href="users.php" class="auto">
                            <i class="i i-user3"></i>
                            <span>Users</span>
                          </a>
                        </li>
                        <li >
                          <a href="posts.php" class="auto">
                            <i class="i i-chat3"></i>
                            <span>Chattrs</span>
                          </a>
                        </li>
                         <li >
                          <a href="picks.php" class="auto">
                            <i class="fa fa-star-o"></i>
                            <span>Peeks</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            <footer class="footer hidden-xs no-padder text-center-nav-xs">
              <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                <i class="i i-logout"></i>
              </a>
              <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                <i class="i i-circleleft text"></i>
                <i class="i i-circleright text-active"></i>
              </a>
            </footer>
          </section>
        </aside>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Users</h3>
                    </div>
                    <div class="col-sm-6 text-right text-left-xs m-t-md">
                      <a href="#nav, #sidebar" class="btn btn-icon b-2x btn-info btn-rounded" data-toggle="class:nav-xs, show"><i class="fa fa-bars"></i></a>
                    </div>
                  </section>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                    <section class="panel panel-default">
                  <header class="panel-heading">
                  User Details
                </header>

                 <?php
include "rest.php";

$url = "http://45.55.229.81/chattr/api/v1/users";
$cht = curl_init($url);
curl_setopt($cht, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($cht);
$info = json_decode($response);
curl_close($cht);
echo '<div class="table-responsive">';
echo '<table class="table table-striped m-b-none sortable">';
echo '<thead>
                      <tr>
                        <th width="15%">Username</th>
                        <th width="20%">Email</th>
                        <th width="25%">Location</th>
                        <th width="15%">Status</th>
                        <th width="10%">Ban/Unban</th>
                        <th width="10%">Chattrs</th>
                        <th width="5%">Delete User</th>
                      </tr>
                    </thead>
                    <tbody>';

foreach($info as $item)
  {
  echo "<form method='POST' action='rest.php?id=" . $item->{'id'} . "' >";
    echo "<tr><td><a href='profile.php?id=" . urlencode($item->{'id'}) . "'>" . $item->{'username'} . "</a></td>";
        echo "<td>" . $item->{'email'} . "</td>";
          
          $purl = "http://maps.google.com/maps/api/geocode/json?latlng=" . $item->{'lat'} . "," . $item->{'lng'} . "&sensor=false";
          $response = file_get_contents($purl);
          $data = json_decode($response);
              if ($data->status == "ZERO_RESULTS")
                {
                echo "<td>heaven</td>";
                }
                else
                {
                echo "<td>" . substr($data->results[0]->formatted_address, 0, 20) . " " . $data->results[2]->formatted_address . "</td>";
                }

              if ($item->{'banned'} == 1)
                  {
                  echo "<td>banned</td>";
                  echo "<td><button type='submit' name='unbanuser' class='btn btn-default' ><i class= 'fa fa-unlock'></i></td>";
                  }
                  else
                  {
                  echo "<td> active</td>";
                  echo "<td><button type='submit' name='banuser' class='btn btn-default'><i class= 'fa fa-unlock-alt'></i></td>";
                  }

                $client = curl_init($url . "/" . $item->{'id'} . "/posts");
                  curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
                  $response = curl_exec($client);
                  $result = json_decode($response);
                  $userposts = sizeof($result, 1);
                  echo "<td>" . $userposts . "  <i class ='i i-bubble'></a></td>";
                  curl_close($client);
                  echo "<td><button type='submit' name='deleteuser' class='btn btn-default' id='form2'><i class= 'fa fa-trash-o'></i></td> ";
                  echo "</form>";
                  }

                echo "</table>";
?>
                    </section>
                  </div>
                </div>
              </section>
            </section>
            <!-- side content -->
            <!-- / side content -->
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="js/calendar/demo.js"></script>
  <script src="js/sortable/jquery.sortable.js"></script>
  <script src="js/app.plugin.js"></script>

  </body>
</html>