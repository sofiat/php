<?php
session_start();
include "rest.php";

?>
<!DOCTYPE html>
<html lang="en" class="app">
  <head>  
    <meta charset="utf-8" />
    <title>Chattr | Admin Panel</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />  
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" />
    <link rel="stylesheet" href="js/jvectormap/jquery-jvectormap-1.2.2.css" type="text/css" />
    <script src="js/sorttable.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
  </head>
  <body>
    <section class="vbox">
      <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
        <div class="navbar-header aside-md dk">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
        <i class="fa fa-bars"></i>
        </a>
        <a href="index.php" class="navbar-brand">
          <img src="images/logo.png" class="m-r-sm" alt="scale">
          <span class="hidden-nav-xs">Chattr</span>
        </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
          <i class="fa fa-cog"></i>
        </a>
      </div>    
      <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
            </span>
            <input type="text" class="form-control input-sm no-border" placeholder="Search">            
          </div>
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">       
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-left">
              <img src="images/a0.png" alt="...">
            </span>
            <?php
echo $_SESSION['name']; ?>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu animated fadeInRight">         
            <li>
              <a href="signin.php" >Logout</a>
            </li>
          </ul>
        </li>
      </ul>      
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">          
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <div class="clearfix wrapper dk nav-user hidden-xs">
                  <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="thumb avatar pull-left m-r">                        
                        <img src="images/a0.png" class="dker" alt="...">
                        <i class="on md b-black"></i>
                      </span>
                      <span class="hidden-nav-xs clear">
                        <span class="block m-t-xs">
                          <strong class="font-bold text-lt">
                          <?php
echo $_SESSION['name'];
?>
                          </strong> 
                          <b class="caret"></b>
                        </span>
                      
                      </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">                             
                      <li>
                        <a href="signin.php">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>                
                <!-- nav -->                 
                <nav class="nav-primary hidden-xs">
                  <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
                  <ul class="nav nav-main" data-ride="collapse">
                    <li>
                      <a href="index.html" class="auto">
                        <i class="i i-statistics icon">
                        </i>
                        <span class="font-bold">Overview</span>
                      </a>
                    </li>
                    <li class='active' >
                      <a href="users.php" class="auto">                               
                        <i class="i i-user3"></i>
                        <span>Users</span>
                      </a>
                    </li>
                    <li>
                      <a href="posts.php" class="auto">                                                        
                        <i class="i i-chat3"></i>
                        <span>Chattrs</span>
                      </a>
                    </li>
                    <li>
                      <a href="peeks.php" class="auto"><i class="fa fa-star-o"></i><span>peeks</span></a>
                    </li>
                  </ul>
                  </li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
            <footer class="footer hidden-xs no-padder text-center-nav-xs">
              <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                <i class="i i-logout"></i>
              </a>
              <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                <i class="i i-circleleft text"></i>
                <i class="i i-circleright text-active"></i>
              </a>
            </footer>
          </section>
        </aside>
        <section id="content">
          <section class="vbox">
            <section class="scrollable">
              <section class="hbox stretch">
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">
                        <section class="panel no-border bg-primary lt">
                          <div class="panel-body">
                            <div class="row m-t-xl">
                              
                              <div class="col-xs-12 text-center">
                                <div class="inline">
                                  <div class="easypiechart" data-percent="75" data-line-width="6" data-bar-color="#fff" data-track-Color="#2796de" data-scale-Color="false" data-size="140" data-line-cap='butt' data-animate="1000">
                                    <div class="thumb-lg avatar">
                                      <img src="images/a5.png" class="dker">
                                    </div>
                                  </div>
                                <?php

if (isset($_GET['id']))
  {
  $id = $_GET['id'];
  }

$url = "http://45.55.229.81/chattr/api/v1/users/" . $id;
$cht = curl_init($url);
curl_setopt($cht, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($cht);
global $info;
$info = json_decode($response);
curl_close($cht);
?>
<div class="h4 m-t m-b-xs font-bold text-lt"><?php echo $info->{'username'}; ?></div>
                              </div>
                            </div>  
                          </div>
                            <div class="wrapper m-t-xl m-b">
                              <div class="row m-b">
                                <div class="col-xs-6 text-right">
                                  <small>Email</small>
                                  <div class="text-lt font-bold"><?php
echo $info->{'email'} ?></div>
                                </div>
                                <div class="col-xs-6">
                                  <small>Last Known Location</small>
                                  <div class="text-lt font-bold"><?php
    $purl = "http://maps.google.com/maps/api/geocode/json?latlng=" . $info->{'lat'} . "," . $info->{'lng'} . "&sensor=false";
    $response = file_get_contents($purl);
    $data = json_decode($response);
        if ($data->status == "ZERO_RESULTS")
          {
          echo "<td>Unknown Place</td>";
          }
          else
          {
          echo "<td>" . substr($data->results[0]->formatted_address, 0, 20) . " " . $data->results[2]->formatted_address . "</td>";
          }

?>
                                </div>
                              </div>
                            </div>    
                          </div>
                          </div>
                          <footer class="panel-footer dk text-center no-border">
                            <div class="row pull-out">
                              <div class="col-xs-4">
                                <div class="padder-v">
                                  <span class="m-b-xs h3 block text-white">
                                  <?php
        if ($info->{'banned'} == true)
            {
            echo "Banned";
            }
            else
            {
            echo "Active";
            }

?>
                                  </span>
                                  <small class="text-muted">status</small>
                                </div>
                              </div>
                              <div class="col-xs-8 dker">
                                <div class="padder-v">
                                  <span class="m-b-xs h3 block text-white">
                                    <?php
          $id = $_GET['id'];
          $client = curl_init("http://45.55.229.81/chattr/api/v1/users/" . $id . "/posts");
          curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
          $response = curl_exec($client);
          $result = json_decode($response);
          $userposts = sizeof($result, 1);
          echo $userposts;
?>
                                  </span>
                                  <small class="text-muted">Chattrs</small>
                                </div>
                              </div>
                            </div>
                          </footer>
                        </section>
                        <div class="clear">
                          <a href="users.php" class="btn btn-xs btn-info m-t-xs"><i class='fa fa-arrow-left'></i> Back to Users</a>
                        </div>
                        </div> 
                      </div>
                    </section>
                  </section>
                </aside>

                <aside class="col-lg-6 b-l no-padder">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">       
                        <section class="panel panel-default">
                          <h4 class="padder">Chattrs</h4>
                            <?php
          if (isset($_GET['id']))
            {
            $id = $_GET['id'];
            $url = "http://45.55.229.81/chattr/api/v1/users/" . $id . "/posts";
            $cht = curl_init($url);
            curl_setopt($cht, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($cht);
            $info = json_decode($response);
            curl_close($cht);
            echo '<ul class="list-group">';
            foreach($info as $item)
              {
              $post = $item->{'body'};
                $time = $item->{'timePosted'};
                  echo "<li class='list-group-item'>";
                  echo "<p>" . $item->{'body'} . "</p>";
                  echo "<small class='block text-muted'><i class='fa fa-clock-o'></i> " . date('Y-m-d h:i:sa', strtotime($item->{'timePosted'})) . " </small>";
                  echo "</li>";
              }

                  echo "</ul>";
                    }

?>
                        </section>                     
                      </div>
                    </section>
                  </section>              
                </aside>
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>  
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="js/charts/flot/jquery.flot.min.js"></script>
  <script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="js/charts/flot/jquery.flot.spline.js"></script>
  <script src="js/charts/flot/jquery.flot.pie.min.js"></script>
  <script src="js/charts/flot/jquery.flot.resize.js"></script>
  <script src="js/charts/flot/jquery.flot.grow.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script src="js/jvectormap/jquery-jvectormap-us-aea-en.js"></script> 
  <script src="js/calendar/bootstrap_calendar.js"></script>
  <script src="js/sortable/jquery.sortable.js"></script>
  <script src="js/app.plugin.js"></script>

</body>
</html>