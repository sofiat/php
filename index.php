<?php
session_start();
include "rest.php";

?>
<!DOCTYPE html>
<html lang="en" class="app">
  <head>

    <meta charset="utf-8" />
    <title>Chattr | Admin Panel</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/icon.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <link rel="stylesheet" href="js/jvectormap/jquery-jvectormap-1.2.2.css" type="text/css" />
    <script src="js/sorttable.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>

  </head>
  <body>

    <section class="vbox">
      <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
        <div class="navbar-header aside-md dk">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"><i class="fa fa-bars"></i></a>
          <a href="index.php" class="navbar-brand"><img src="images/logo.png" class="m-r-sm" alt="scale">
          <span class="hidden-nav-xs">Chattr</span></a>
          <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"><i class="fa fa-cog"></i></a>
        </div>
        <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-btn">
              <button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>
              </span>
              <input type="text" class="form-control input-sm no-border" placeholder="Search">
            </div>
          </div>
        </form>
        <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="thumb-sm avatar pull-left">
          <img src="images/a0.png" alt="...">
          </span>
          <?php
          echo $_SESSION['name']; ?>
          <b class="caret"></b>
          </a>
            <ul class="dropdown-menu animated fadeInRight">
              <li>
              <a href="signin.php" >Logout</a>
              </li>
            </ul>
        </ul>
      </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <div class="clearfix wrapper dk nav-user hidden-xs">
                  <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-nav-xs clear">
                        <span class="block m-t-xs">
                          <strong class="font-bold text-lt">
                          <?php
                          echo $_SESSION['name'];
                          ?>
                          </strong>
                          <b class="caret"></b>
                        </span>
                      </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                       <li>
                        <a href="signin.php">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
                  <ul class="nav nav-main" data-ride="collapse">
                    <li  class="active">
                      <a href="index.html" class="auto">
                        <i class="i i-statistics icon">
                        </i>
                        <span class="font-bold">Overview</span>
                      </a>
                    </li>
                        <li >
                          <a href="users.php" class="auto">
                            <i class="i i-user3"></i>
                            <span>Users</span>
                          </a>
                        </li>
                        <li >
                          <a href="posts.php" class="auto">
                            <i class="i i-chat3"></i>
                            <span>Chattrs</span>
                          </a>
                        </li>
                        <li >
                          <a href="peeks.php" class="auto">
                            <i class="fa fa-star-o"></i>
                            <span>Peeks</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
            <footer class="footer hidden-xs no-padder text-center-nav-xs">
              <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
                <i class="i i-logout"></i>
              </a>
              <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
                <i class="i i-circleleft text"></i>
                <i class="i i-circleright text-active"></i>
              </a>
            </footer>
          </section>
        </aside>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Dashboard</h3>
                      <small>Welcome back, <?php
                       echo $_SESSION['name'];
                      ?></small>
                    </div>
                    <div class="col-sm-6 text-right text-left-xs m-t-md">
                      <a href="#nav, #sidebar" class="btn btn-icon b-2x btn-info btn-rounded" data-toggle="class:nav-xs, show"><i class="fa fa-bars"></i></a>
                    </div>
                  </section>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel b-a">
                        <div class="row m-n">
                          <div class="col-md-4 b-b b-r">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-danger hover-rotate"></i>
                                <i class="i i-plus2 i-1x text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-danger"><?php
                                echo getNumbers(USERS_API_URL); ?>
                                </span>
                                <small class="text-muted text-u-c">REGISTERED USERS</small>
                              </span>
                            </a>
                          </div>
                          <div class="col-md-4 b-b b-r">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-info hover-rotate"></i>
                                <i class="i i-chat3 i-sm text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-info"><?php
                                echo getNumbers(POST_API_URL); ?>
                                </span>
                                <small class="text-muted text-u-c">Chattrs</small>
                              </span>
                            </a>
                          </div>
                          <div class="col-md-4 b-b">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-success-lt hover-rotate"></i>
                                <i class="i i-users2 i-sm text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-success"><?php
                                echo getNumbers(hoursAgo); ?>
                                </span>
                                <small class="text-muted text-u-c">USERS IN THE LAST 4 HOURS</small>
                              </span>
                            </a>
                          </div>
                          <div class="col-md-4 b-b b-r">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-success hover-rotate"></i>
                                <i class="i i-users2 i-sm text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-info"><?php
                                echo getNumbers(dayAgo); ?>
                                </span>
                                <small class="text-muted text-u-c">SIGNUPS IN THE LAST 24 HOURS</small>
                              </span>
                            </a>
                          </div>
                          <div class="col-md-4 b-b b-r">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-success hover-rotate"></i>
                                <i class="i i-users2 i-sm text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-info"><?php
                                echo getNumbers(weekAgo); ?>
                                </span>
                                <small class="text-muted text-u-c">SIGNUPS IN THE LAST 7 DAYS</small>
                              </span>
                            </a>
                          </div>
                          <div class="col-md-4 b-b b-r">
                            <a href="#" class="block padder-v hover">
                              <span class="i-s i-s-2x pull-left m-r-sm">
                                <i class="i i-hexagon2 i-s-base text-success hover-rotate"></i>
                                <i class="i i-users2 i-sm text-white"></i>
                              </span>
                              <span class="clear">
                                <span class="h3 block m-t-xs text-info"><?php
                                echo getNumbers(monthAgo); ?>
                                </span>
                                <small class="text-muted text-u-c">SIGNUPS IN THE LAST 30 DAYS</small>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <section class="vbox">
                  <section class="scrollable wrapper">
                    <div class="panel b-a">
                      <div class="panel-heading b-b">Chattrs</div>
                        <div class="panel-body">
                          <div class="h3 m-b font-thin">Location of chattrs</div>
                            <div class="row">
                              <div class="col-sm-12">
                                <div id="map_canvas" style=' height:400px; width:100%'>
                                </div>
                                <div class="m-t-xl m-b clearfix">
                                  <i class="i i-local i-2x text-info pull-left m-r m-l m-t-xs"></i>
                                  <div class="clear text-sm">
                                  This map shows the location of chattr users
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </section>
                </section>
              </section>
            </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>

  <script>
    function initialize() {
      var mapCanvas = document.getElementById('map_canvas');
      var mapOptions = {
      center: new google.maps.LatLng(6.506189, 3.378062),
      zoom: 16
      }

      var map = new google.maps.Map(mapCanvas, mapOptions);
      var json = (function () {
      var json = null;
      $.ajax({
            'async': false,
            'global': false,
            'url': "http://45.55.229.81/chattr/api/v1/posts/all",
            'dataType': "json",
            'success': function (data) {
            json = data;
            }
            });
            return json;
            })();

      var goldStar = {
      path: 'M-5,0a5,5 0 1,0 10,0a5,5 0 1,0 -10,0',
      fillColor: 'purple',
      fillOpacity: 1,
      scale: 1.5,
      strokeColor: 'white',
      strokeWeight: 1
      };
      for (var i = 0, length = json.length; i < length; i++) {
          var data = json[i],
          latLng = new google.maps.LatLng(data.lat, data.lng);

            // Creating a marker and putting it on the map

          var marker = new google.maps.Marker({
              icon: goldStar,
              position: latLng,
              map: map,
              title: data.id
            });
          }
      }
      google.maps.event.addDomListener(window, "load", initialize);
  </script>

  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script>
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="js/sortable/jquery.sortable.js"></script>
  <script src="js/app.plugin.js"></script>

  </body>
</html>